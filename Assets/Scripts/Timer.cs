﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

    public static Timer instance;

    public GameObject StartButton;

    public Text TimerText;
    public float timeInSeconds = 3.0f * 60f;

    public bool startTimer = false;
    [HideInInspector]
    public bool gameOver = false;

    public float currentTime;
    // Use this for initialization

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Start () {
      
        currentTime = timeInSeconds;
	}
	
	// Update is called once per frame
	void Update () {

        if (startTimer)
        {
            if (currentTime > 0f)
            {
                currentTime -= Time.deltaTime;
            }
            else
            {
                currentTime = 0f;
                startTimer = false;
                gameOver = true;
                OfflineTimer.instance.startCoolDownTimer = true;
            }
        }
        TimerText.text = "Time Left: " + string.Format("{0}:{1:00}", Mathf.FloorToInt(currentTime / 60), currentTime % 60);
        if (gameOver)
        {
            ScoreTracker.instance.SaveHighScore();
            StartButton.SetActive(true);
            StartButton.GetComponent<Button>().interactable = false;
            


        }
	}

    public void StartTimer()
    {
        startTimer = true;
        gameOver = false;
        currentTime = timeInSeconds;
        ScoreTracker.instance.score = 0;
    }
}
