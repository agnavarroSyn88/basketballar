﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class OfflineTimer : MonoBehaviour {

    public static OfflineTimer instance;
    public const string COOLDOWN_TIMER_STRING = "KEY_COOLDOWN_TIMER_STRING";
    public const string COOLDOWN_TIMER_FLOAT = "KEY COOLDOWN_TIMER_FLOAT";
    public const string GAMEOVER = "KEY_GAMEOVER";
    public float timeInSeconds = 300f;
    public string lastSavedTime;
    public float currentTime;
    public bool startCoolDownTimer = false;

    System.DateTime savedTime;
    // Use this for initialization

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);



        if (PlayerPrefs.HasKey(GAMEOVER))
        {
            if (PlayerPrefs.GetInt(GAMEOVER) == 1)
            {
                Timer.instance.gameOver = true;
                startCoolDownTimer = true;
            }
            else
            {
                Timer.instance.gameOver = false;
                startCoolDownTimer = false;
            }
        }
    }
    
	void Start () {
        

        if(Timer.instance.gameOver)
        {
            if (PlayerPrefs.HasKey(COOLDOWN_TIMER_STRING))
            {
                lastSavedTime = PlayerPrefs.GetString(COOLDOWN_TIMER_STRING);
                savedTime = System.DateTime.Parse(lastSavedTime);
            }
            if (PlayerPrefs.HasKey(COOLDOWN_TIMER_FLOAT))
            {
                currentTime = PlayerPrefs.GetFloat(COOLDOWN_TIMER_FLOAT);
            }

            float timePassedOffline = (float)(System.DateTime.Now - savedTime).TotalSeconds;
            currentTime -= timePassedOffline;
        }
        else
        {
            currentTime = timeInSeconds;
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (startCoolDownTimer)
        {
            if (currentTime > 0f)
            {
                currentTime -= Time.deltaTime;
            }
            else
            {
                currentTime = 0f;
                startCoolDownTimer = false;
            }
            Timer.instance.StartButton.GetComponentInChildren<Text>().text = string.Format("{0}:{1:00}", Mathf.FloorToInt(currentTime / 60), currentTime % 60);
        }
        else
        {
            Timer.instance.gameOver = false;
            Timer.instance.StartButton.GetComponent<Button>().interactable = true;
            Timer.instance.StartButton.GetComponentInChildren<Text>().text = "Start";
            currentTime = timeInSeconds;
        }
	
	}

    void OnApplicationQuit()
    {
        PlayerPrefs.SetString(COOLDOWN_TIMER_STRING, System.DateTime.Now.ToString());
        PlayerPrefs.SetFloat(COOLDOWN_TIMER_FLOAT, currentTime);
        if (Timer.instance.gameOver)
            PlayerPrefs.SetInt(GAMEOVER, 1);
        else
            PlayerPrefs.SetInt(GAMEOVER, 0);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            PlayerPrefs.SetString(COOLDOWN_TIMER_STRING, System.DateTime.Now.ToString());
            PlayerPrefs.SetFloat(COOLDOWN_TIMER_FLOAT, currentTime);
            if (Timer.instance.gameOver)
                PlayerPrefs.SetInt(GAMEOVER, 1);
            else
                PlayerPrefs.SetInt(GAMEOVER, 0);
        }
        else
        {

            if (PlayerPrefs.HasKey(GAMEOVER))
            {
                if (PlayerPrefs.GetInt(GAMEOVER) == 1)
                {
                    Timer.instance.gameOver = true;
                    startCoolDownTimer = true;
                }
                else
                {
                    Timer.instance.gameOver = false;
                    startCoolDownTimer = false;
                }
            }

            if (Timer.instance.gameOver)
            {
                if (PlayerPrefs.HasKey(COOLDOWN_TIMER_STRING))
                {
                    lastSavedTime = PlayerPrefs.GetString(COOLDOWN_TIMER_STRING);
                    savedTime = System.DateTime.Parse(lastSavedTime);
                }
                if (PlayerPrefs.HasKey(COOLDOWN_TIMER_FLOAT))
                {
                    currentTime = PlayerPrefs.GetFloat(COOLDOWN_TIMER_FLOAT);
                }

                float timePassedOffline = (float)(System.DateTime.Now - savedTime).TotalSeconds;
                currentTime -= timePassedOffline;
            }
            else
            {
                currentTime = timeInSeconds;
            }
        }
        
    }
}
