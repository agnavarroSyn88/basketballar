﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComputeCameraDistance : MonoBehaviour {

    public Text Distance;

    public GameObject ImageTarget;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float distance = Mathf.Abs(transform.position.z - ImageTarget.transform.position.z);
        Distance.text = "Distance from camera: " + distance.ToString();
	
	}
}
