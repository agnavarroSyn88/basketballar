﻿using UnityEngine;
using System.Collections;

public class TapForce : MonoBehaviour {

    public GameObject cameraAR;
    public GameObject imageTarget;
    public GameObject placeholder;
   
    Vector3 origPos;
    Quaternion origQuat;

    public float throwSpeed = 1500f;
    private float speed;
    private float lastMouseX, lastMouseY;

    private bool thrown, holding;

    private Rigidbody _rigidbody;
    private Vector3 newPosition;

    
	// Use this for initialization
	void Start () {
        origPos = transform.position;
        origQuat = transform.rotation;
        
        _rigidbody = GetComponent<Rigidbody>();
        Reset();

    }


    // Update is called once per frame
    void Update () {

        

        if (holding)
            OnTouch();
       

        if (thrown)
            return;

        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        { //for pc = if(Input.GetButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); //for pc = Input.mousePosition
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.transform == transform)
                {
                    holding = true;
                    transform.SetParent(imageTarget.transform);
                }
            }
        }

        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
        { //for pc = if(Input.GetButtonUp(0)){
            if (lastMouseY < Input.GetTouch(0).position.y)
            {
                ThrowBall(Input.GetTouch(0).position);
            }
        }

        if (Input.touchCount == 1)
        { //for pc = if(Input.GetButton(0)){
            lastMouseX = Input.GetTouch(0).position.x;
            lastMouseY = Input.GetTouch(0).position.y;
        }

    }

   
    void OnTouch()
    {
        Vector3 mousePos = Input.GetTouch(0).position;
        mousePos.z = placeholder.transform.position.z;

        newPosition = Camera.main.ScreenToWorldPoint(mousePos);

        transform.position = Vector3.Lerp(transform.position, newPosition, 10f * Time.deltaTime);
    }

    void ThrowBall(Vector2 mousePos)
    {
        
        _rigidbody.useGravity = true;

        float differenceY = (mousePos.y - lastMouseY) / Screen.height * 100;
        speed = throwSpeed * differenceY;

        float x = (mousePos.x / Screen.width) - (lastMouseX / Screen.width);
        x = Mathf.Abs(Input.GetTouch(0).position.x - lastMouseX) / Screen.width * 100 * x;

        Vector3 direction = new Vector3(x, 0f, 1f);
        direction = Camera.main.transform.TransformDirection(direction);

        _rigidbody.AddForce((direction * speed * 0.5f) + (Vector3.up * speed));

        holding = false;
        thrown = true;
        placeholder.GetComponent<SpawnBalls>().isCurrentBallThrown = true;

        DestroyObject(gameObject, 8f);
        //Invoke("Reset", 5.0f);
    }

    void Reset()
    {
        CancelInvoke();
        transform.position = placeholder.transform.position;
        newPosition = transform.position;
        thrown = holding = false;
        GetComponent<Renderer>().enabled = true;
        GetComponent<Collider>().enabled = true;
        _rigidbody.useGravity = false;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        transform.rotation = cameraAR.transform.rotation;
        transform.SetParent(cameraAR.transform);
    }
}
