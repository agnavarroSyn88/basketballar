﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreTracker : MonoBehaviour {

    public const string HIGH_SCORE_KEY = "HighScore";

    public static ScoreTracker instance;

    public Text textScore;
    public Text textHighScore;
    [SerializeField]
    public int score;
    public int highScore;

	// Use this for initialization
	void Start () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        if (!PlayerPrefs.HasKey(HIGH_SCORE_KEY))
        {
            PlayerPrefs.SetInt(HIGH_SCORE_KEY, score);
        }
        else
        {
            highScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY);
        }
        
    }
	
	// Update is called once per frame
	void Update () {
        textScore.text = "Score: " + score.ToString();
        textHighScore.text = "High Score: " + highScore.ToString();
    }

    public void SaveHighScore()
    {
        if(score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt(HIGH_SCORE_KEY, highScore);
        }
    }
}
