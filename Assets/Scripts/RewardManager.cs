﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RewardManager : MonoBehaviour {

    public string BB8BundleID;

	// Use this for initialization
	void Start () {
        if (isAppInstalled(BB8BundleID))
        {
            CollectReward();
        }
        else
        {
            GetComponent<Image>().color = Color.red;
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool isAppInstalled(string bundleID)
    {
    #if UNITY_ANDROID
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
            Debug.Log(" ********LaunchOtherApp ");
            AndroidJavaObject launchIntent = null;
            //if the app is installed, no errors. Else, doesn't get past next line
            try
            {
                launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID);
                //        
                //        ca.Call("startActivity",launchIntent);
            }
            catch (System.Exception ex)
            {
                Debug.Log("exception" + ex.Message);
            }
            if (launchIntent == null)
                return false;
            return true;
    #else
             return false;
    #endif
    }

    void CollectReward()
    {
        //implement reward collection
        GetComponent<Image>().color = Color.green;
    }
}
