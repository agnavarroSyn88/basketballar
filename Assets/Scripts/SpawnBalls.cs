﻿using UnityEngine;
using System.Collections;

public class SpawnBalls : MonoBehaviour {

    public GameObject basketball;
    public GameObject cameraAR;
    public GameObject imageTarget;

    [HideInInspector]
    public bool isCurrentBallThrown = false;

    GameObject currentBall;

	// Use this for initialization
	void Start () {

       
      
	
	}
	
	// Update is called once per frame
	void Update () {

        if (isCurrentBallThrown)
        {
            StartCoroutine(SpawnBall());
        }
	
	}

    void CreateBall()
    {
        currentBall = Instantiate(basketball, transform.position, cameraAR.transform.rotation) as GameObject;
        currentBall.GetComponent<TapForce>().cameraAR = cameraAR;
        currentBall.GetComponent<TapForce>().placeholder = gameObject;
        currentBall.GetComponent<TapForce>().imageTarget = imageTarget;
    }

    IEnumerator SpawnBall()
    {
        isCurrentBallThrown = false;
        yield return new WaitForSeconds(1f);
        if(!Timer.instance.gameOver && Timer.instance.startTimer)
            CreateBall();
        
    }

    public void StartSpawning()
    {
        CreateBall();
    }
}
